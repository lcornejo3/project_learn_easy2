package login.bean;

import org.hibernate.Session;
import org.hibernate.Transaction;


import login.bean.LoginB;
import login.database.LoginConnect;


public class UserValidation {
    public void saveUser(LoginB user) {
        Transaction transaction = null;
        try (Session session = LoginConnect.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the student object
            session.save(user);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
    public boolean validate(String userName, String password) {

        Transaction transaction = null;
        LoginB user = null;
        try (Session session = LoginConnect.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an user object
            user = (LoginB) session.createQuery("FROM USER U WHERE U.username = :userName").setParameter("userName", userName)
                    .uniqueResult();

            if (user != null && user.getPassword().equals(password)) {
                return true;
            }
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return false;
    }
}
